FROM golang:alpine as build

WORKDIR /go/src/gitlab.com/shagon/zephyr
EXPOSE 80
EXPOSE 443

COPY . ./

RUN apk add --no-cache git \
    && go get -d -v ./... \
    && go install .

FROM alpine as run

WORKDIR /app
COPY --from=build /go/bin/zephyr .
COPY example.env .env
COPY website website
COPY assets assets

CMD [ "/app/zephyr" ]
