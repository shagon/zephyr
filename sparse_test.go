package main

import (
	"fmt"
	"os"
	"strconv"
	"testing"
)

func Test_sparse(t *testing.T) {
	testCases := []struct {
		desc      string
		size      string
		humanSize string
	}{
		{
			desc:      "valid size",
			size:      "2MB",
			humanSize: "2000000",
		},
		{
			desc:      "invalid size",
			size:      "random",
			humanSize: "1",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			loc := sparse(tC.size)
			info, err := os.Stat(loc)
			if err != nil {
				t.Errorf("failed to open file at %s; %v", loc, err)
			}

			size := strconv.FormatInt(info.Size(), 10)
			if size != tC.humanSize {
				t.Errorf("expected size of %s, got %s", tC.humanSize, size)
			}

			filename := info.Name()
			if filename != fmt.Sprintf("file-%s", tC.size) {
				t.Errorf("expected name to be file-%s, got %s", tC.size, filename)
			}
		})
	}
}
