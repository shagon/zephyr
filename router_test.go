package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_router(t *testing.T) {
	srv := httptest.NewServer(makeRouter())
	defer srv.Close()

	testCases := []struct {
		desc       string
		httpMethod string
		route      string
		statusCode int
	}{
		{
			desc:       "should return 404",
			httpMethod: http.MethodGet,
			route:      "/api/v1/404",
			statusCode: http.StatusNotFound,
		},
		{
			desc:       "should return apidoc",
			httpMethod: http.MethodGet,
			route:      "/",
			statusCode: http.StatusOK,
		},
		{
			desc:       "health check",
			httpMethod: http.MethodGet,
			route:      "/api/v1/health",
			statusCode: http.StatusOK,
		},
		{
			desc:       "bench check",
			httpMethod: http.MethodGet,
			route:      "/api/v1/bench",
			statusCode: http.StatusOK,
		},
		{
			desc:       "chat page check",
			httpMethod: http.MethodGet,
			route:      "/chat",
			statusCode: http.StatusOK,
		},
		{
			desc:       "video page check",
			httpMethod: http.MethodGet,
			route:      "/video",
			statusCode: http.StatusOK,
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			req, err := http.NewRequest(tC.httpMethod, fmt.Sprintf("%s%s", srv.URL, tC.route), nil)
			if err != nil {
				t.Fatalf("could not create request %s", err)
			}

			client := &http.Client{}
			res, err := client.Do(req)

			if err != nil {
				t.Fatalf("could not send request: %v", err)
			}
			defer res.Body.Close()

			if res.StatusCode != tC.statusCode {
				t.Errorf("expected status %d; got %v", tC.statusCode, res.Status)
			}
		})
	}
}

func Test_basicHttpAuth(t *testing.T) {
	srv := httptest.NewServer(makeRouter())
	defer srv.Close()

	testCases := []struct {
		desc       string
		httpMethod string
		statusCode int
		username   string
		pass       string
	}{
		{
			desc:       "should return 401",
			httpMethod: http.MethodGet,
			statusCode: http.StatusUnauthorized,
		},
		{
			desc:       "should return 401 because of wrong pass",
			httpMethod: http.MethodGet,
			statusCode: http.StatusUnauthorized,
			username:   "user",
			pass:       "123",
		},
		{
			desc:       "should return 401 due to wrong header",
			httpMethod: http.MethodGet,
			statusCode: http.StatusUnauthorized,
			username:   "user",
		},
		{
			desc:       "should return 200",
			httpMethod: http.MethodGet,
			statusCode: http.StatusOK,
			username:   "user",
			pass:       "pass",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			req, err := http.NewRequest(tC.httpMethod, fmt.Sprintf("%s/api/v1/basic", srv.URL), nil)
			if err != nil {
				t.Fatalf("could not create request %s", err)
			}

			req.SetBasicAuth(tC.username, tC.pass)

			if tC.username != "" && tC.pass == "" {
				req.Header.Set("Authorization", "random_string")
			}

			client := &http.Client{}
			res, err := client.Do(req)

			if err != nil {
				t.Fatalf("could not send request: %v", err)
			}
			defer res.Body.Close()

			if res.StatusCode != tC.statusCode {
				t.Errorf("expected status %d; got %v", tC.statusCode, res.Status)
			}
		})
	}
}

func Test_echoRequest(t *testing.T) {
	srv := httptest.NewServer(makeRouter())
	defer srv.Close()

	testCases := []struct {
		desc        string
		httpMethod  string
		route       string
		statusCode  int
		headerKey   string
		headerValue string
		body        string
	}{
		{
			desc:        "echo request header",
			httpMethod:  http.MethodGet,
			route:       "/api/v1/echo",
			statusCode:  http.StatusOK,
			headerKey:   "Test",
			headerValue: "Value",
		},
		{
			desc:       "echo status code",
			httpMethod: http.MethodGet,
			route:      "/api/v1/echo?status=418",
			statusCode: http.StatusTeapot,
		},
		{
			desc:       "echo body",
			httpMethod: http.MethodPost,
			route:      "/api/v1/echo",
			statusCode: http.StatusOK,
			body:       "test body",
		},
		{
			desc:       "echo throttleRequest",
			httpMethod: http.MethodGet,
			route:      "/api/v1/echo?after=2",
			statusCode: http.StatusOK,
		},
		{
			desc:        "echo xff",
			httpMethod:  http.MethodGet,
			route:       "/api/v1/echo",
			statusCode:  http.StatusOK,
			headerKey:   "X-Forwarded-For",
			headerValue: "1.2.3.4",
		},
		{
			desc:        "echo x-real-ip",
			httpMethod:  http.MethodGet,
			route:       "/api/v1/echo",
			statusCode:  http.StatusOK,
			headerKey:   "X-Real-Ip",
			headerValue: "1.2.3.4",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			req, err := http.NewRequest(tC.httpMethod, fmt.Sprintf("%s%s", srv.URL, tC.route), nil)
			if err != nil {
				t.Fatalf("could not create request %s", err)
			}
			req.Body = ioutil.NopCloser(strings.NewReader(tC.body))
			if tC.headerKey != "" {
				req.Header.Add(tC.headerKey, tC.headerValue)
			}

			client := &http.Client{}
			res, err := client.Do(req)

			if err != nil {
				t.Fatalf("could not send request: %v", err)
			}
			defer res.Body.Close()

			if res.StatusCode != tC.statusCode {
				t.Errorf("expected status %d; got %v", tC.statusCode, res.Status)
			}

			b, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Fatalf("could not read response: %v", err)
			}
			var data map[string]interface{}
			err = json.Unmarshal(b, &data)
			if err != nil {
				t.Fatalf("could not convert to struct %v", err)
			}

			if tC.headerKey != "" {
				if data[tC.headerKey] != tC.headerValue {
					t.Errorf("expected %s in response body, got %s", tC.headerKey, data)
				}
			}

			if tC.body != "" {
				if tC.body != data["Body"] {
					t.Errorf("expected %s in response body, got %s", tC.body, data)
				}
			}
		})
	}
}

func Test_chunkedRes(t *testing.T) {
	srv := httptest.NewServer(makeRouter())
	defer srv.Close()

	testCases := []struct {
		desc       string
		httpMethod string
		route      string
		statusCode int
		count      int // used to validate the number of chunks returned
	}{
		{
			desc:       "chunked request",
			httpMethod: http.MethodGet,
			route:      "/api/v1/chunked/2",
			statusCode: http.StatusOK,
			count:      2,
		},
		{
			desc:       "chunked request incorrect param",
			httpMethod: http.MethodGet,
			route:      "/api/v1/chunked/rand_string",
			statusCode: http.StatusOK,
			count:      10,
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			req, err := http.NewRequest(tC.httpMethod, fmt.Sprintf("%s%s", srv.URL, tC.route), nil)
			if err != nil {
				t.Fatalf("could not create request %s", err)
			}

			client := &http.Client{}
			res, err := client.Do(req)

			if err != nil {
				t.Fatalf("could not send request: %v", err)
			}
			defer res.Body.Close()

			if res.StatusCode != tC.statusCode {
				t.Errorf("expected status %d; got %v", tC.statusCode, res.Status)
			}

			b, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Fatalf("could not read response: %v", err)
			}

			numNewlines := countRune(string(b), '\n')
			if numNewlines != tC.count {
				t.Fatalf("expected %v of chunks, got %v", tC.count, numNewlines)
			}
		})
	}
}

func Test_genFile(t *testing.T) {
	srv := httptest.NewServer(makeRouter())
	defer srv.Close()

	testCases := []struct {
		desc  string
		route string
		size  string
	}{
		{
			desc:  "generate file 200 bytes",
			route: "/api/v1/file/200",
			size:  "200",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodHead, fmt.Sprintf("%s%s", srv.URL, tC.route), nil)
			if err != nil {
				t.Fatalf("could not create request %s", err)
			}

			client := &http.Client{}
			res, err := client.Do(req)

			if err != nil {
				t.Fatalf("could not send request: %v", err)
			}
			defer res.Body.Close()

			if res.StatusCode != http.StatusOK {
				t.Errorf("expected status %d; got %v", http.StatusOK, res.Status)
			}

			length := res.Header.Get("Content-Length")

			if length != tC.size {
				t.Errorf("expected %s bytes, got %s", tC.size, length)
			}
		})
	}
}

func Test_loremData(t *testing.T) {
	srv := httptest.NewServer(makeRouter())
	defer srv.Close()

	testCases := []struct {
		desc    string
		route   string
		resType string
	}{
		{
			desc:    "lorem text",
			route:   "/api/v1/lorem/text",
			resType: "text/plain; charset=utf-8",
		},
		{
			desc:    "lorem gzip",
			route:   "/api/v1/lorem/gzip",
			resType: "gzip",
		},
		{
			desc:    "lorem br",
			route:   "/api/v1/lorem/brotli",
			resType: "br",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodHead, fmt.Sprintf("%s%s", srv.URL, tC.route), nil)
			if err != nil {
				t.Fatalf("could not create request %s", err)
			}

			client := &http.Client{}
			res, err := client.Do(req)

			if err != nil {
				t.Fatalf("could not send request: %v", err)
			}
			defer res.Body.Close()

			if res.StatusCode != http.StatusOK {
				t.Errorf("expected status %d; got %v", http.StatusOK, res.Status)
			}

			var cType string
			switch tC.resType {
			case "text/plain; charset=utf-8":
				cType = res.Header.Get("Content-Type")
			case "gzip", "br":
				cType = res.Header.Get("Content-Encoding")
			}

			if cType != tC.resType {
				t.Errorf("expected %s type, got %s", tC.resType, cType)
			}
		})
	}
}

// countRune - counts characters in a string
func countRune(s string, r rune) int {
	count := 0
	for _, c := range s {
		if c == r {
			count++
		}
	}
	return count
}
