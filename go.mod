module zephyr

go 1.21

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.1
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.4.2
)

require (
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
)
