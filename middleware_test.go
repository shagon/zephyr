package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	log "github.com/sirupsen/logrus"
)

func Test_disable_logging(t *testing.T) {
	// disable logrus logging
	log.SetOutput(ioutil.Discard)
}

func Test_middleware(t *testing.T) {
	srv := httptest.NewServer(makeRouter())
	defer srv.Close()

	testCases := []struct {
		desc        string
		httpMethod  string
		route       string
		headerKey   string
		headerValue string
	}{
		{
			desc:        "should return server header",
			httpMethod:  http.MethodGet,
			route:       "/api/v1/echo",
			headerKey:   "server",
			headerValue: "zephyr",
		},
		{
			desc:        "should return CORS",
			httpMethod:  http.MethodGet,
			route:       "/api/v1/echo",
			headerKey:   "Access-Control-Allow-Origin",
			headerValue: "*",
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			req, err := http.NewRequest(tC.httpMethod, fmt.Sprintf("%s%s", srv.URL, tC.route), nil)
			if err != nil {
				t.Fatalf("could not create request %s", err)
			}

			client := &http.Client{}
			res, err := client.Do(req)

			if err != nil {
				t.Fatalf("could not send request: %v", err)
			}
			defer res.Body.Close()

			if res.Header.Get(tC.headerKey) != tC.headerValue {
				t.Errorf("expected key %s; got %v", tC.headerValue, res.Header.Get(tC.headerKey))
			}
		})
	}
}
