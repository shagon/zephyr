# zephyr
*a soft gentle breeze*

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/shagon/zephyr)](https://goreportcard.com/report/gitlab.com/shagon/zephyr)
[![GitLab pipeline](https://img.shields.io/gitlab/pipeline/shagon/zephyr)](https://gitlab.com/shagon/zephyr/builds)
[![pipeline status](https://gitlab.com/shagon/zephyr/badges/master/pipeline.svg)](https://gitlab.com/shagon/zephyr/-/commits/master)
[![docs](https://badgen.net/badge/docs/latest/green)](https://zephyr.redpulsar.dev)
[![release](https://badgen.net/badge/release/v1.2.1/green)](https://gitlab.com/shagon/zephyr/-/releases/v1.2.1)


**Table of Contents**
- [zephyr](#zephyr)
	- [quickstart](#quickstart)
	- [how it works](#how-it-works)
	- [config](#config)
	- [libraries used](#libraries-used)

___

**zephyr** is a webserver written in golang. It has numerous endpoints useful for debugging http/https/websocket traffic.

* **zephyr** has a small footprint, can be served on any port as well as any domain.
* **zephyr** has a dockerfile in case you would like to try it out, the image size is around 50MB (due to static files for testing, e.g. videos).
* **zephyr** uses an `.env` file for a quickstart of the app, you can use the `example.env` file to quickstart.
* **zephyr** also has a helmchart and the kubernetes files located at `k8s` and `helm` folders.
<p align="center">
  <img src="./docs/img/terminal.png" alt="CLI view" width="1024">
</p>

## quickstart

If you want to skip ahead and test what this can do you can either build the app:

```bash
$ git clone https://gitlab.com/shagon/zephyr.git && cd zephyr
$ mv example.env .env
$ go get -v && go build . && ./zephyr
```

Additionally docker containers are also provided. You can find them [here](https://gitlab.com/shagon/zephyr/container_registry) or just run it as:

```bash
$ docker run -it --rm -p 80:80 registry.gitlab.com/shagon/zephyr:master
```

## how it works

1. **zephyr** can be started for http and https traffic, all you need is to supply the domain name and the SSL certificates for https.
2. every request is logged so you can track it in the console, every response includes an `x-request-id` header so you can use it to figure out what request is yours.
3. the main point about it is the ease of use, **zephyr** tries to cover almost every use case that's hard to debug or replicate on normal webservers, e.g. slow responses.

## config

**zephyr** supports few environment parameters, you can edit them in the `.env` file:

* **SSL_CERT**: specify the path to the SSL cert if you wish to use it over https, the path can be absolute e.g. `/home/me/Desktop/cert/fullchain.pem`
* **SSL_KEY**: specify the path to the SSL key, you need this as well as the cert if you need https.
* **HTTP_PORT**: any port that's available, if you wish to map it on a port lower than 1024 you might need `sudo`.
* **HTTPS_PORT**: same as the above.

The online documentation is located at the `/apidoc` endpoint, also **zephyr** has a web interface for the documentation at the root of the project, just go to the homepage and look around, the functions are named and documented as well, for the requests themselves please use the web interface.

## libraries used

This project wouldn't be possible with the help of the following libraries:

* github.com/mkdocs/mkdocs - for the documentation
* github.com/squidfunk/mkdocs-material - the material theme for the documentation
*	github.com/dustin/go-humanize - for human readable values
* github.com/google/uuid - for generating uuids
* github.com/gorilla/mux - router itself
* github.com/gorilla/websocket - websocket support
* github.com/joho/godotenv - easy configuration management
* github.com/sirupsen/logrus - logging software

Icon made by <a href="https://www.flaticon.com/authors/photo3idea-studio" title="photo3idea_studio">photo3idea_studio</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>.