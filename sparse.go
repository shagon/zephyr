package main

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"

	"github.com/dustin/go-humanize"
)

// sparse - function takes 2 parameters, the name (file) and the size (size) and creates an empty file with that size, returns the location of the file
func sparse(size string) string {
	fileName := fmt.Sprintf("file-%s", size)
	pb, err := humanize.ParseBytes(size) // size in bytes
	if err != nil {                      // not valid input
		log.Warnf("Invalid size %s", size)
		pb = 1
	}
	fp := "./assets/files"                   // filePath
	sl := fmt.Sprintf("%s/%s", fp, fileName) // saveLocation

	// if the file exists just return the path to it
	_, err = os.Stat(sl)
	if err == nil {
		return sl
	}

	// create a folder to store the files
	err = os.MkdirAll(fp, 0755)
	if err != nil {
		log.Fatal("Failed to create file")
	}

	fd, err := os.Create(sl)
	if err != nil {
		log.Fatal("Failed to create output")
	}

	_, err = fd.Seek(int64(pb)-1, 0)
	if err != nil {
		log.Fatal("Failed to seek")
	}

	_, err = fd.Write([]byte{0})
	if err != nil {
		log.Fatal("Write operation failed")
	}

	err = fd.Close()
	if err != nil {
		log.Fatal("Failed to close file")
	}

	return fd.Name() // returning the location of the file
}
