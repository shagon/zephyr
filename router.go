package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

var (
	router = makeRouter()
)

// makeRouter - function creates a router instance used by the http/https server
func makeRouter() *mux.Router {
	// init hub for websocket connections
	hub := newHub()
	go hub.run()

	router := mux.NewRouter()

	basic := http.HandlerFunc(httpAuth)
	chatws := http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) { serveWs(hub, res, req) })
	chunked := http.HandlerFunc(chunkedRes)
	echoReq := http.HandlerFunc(echoRequest)
	getFile := http.HandlerFunc(generateFile)
	lorem := http.HandlerFunc(loremData)
	servePage := http.HandlerFunc(templateHTML)
	healthz := http.HandlerFunc(health)
	benchmark := http.HandlerFunc(bench)

	// api calls
	router.Handle("/api/v1/basic", LoggerHandler(basic))
	router.Handle("/api/v1/chunked/{chunks}", LoggerHandler(chunked))
	router.Handle("/api/v1/echo", LoggerHandler(echoReq))
	router.Handle("/api/v1/file/{size}", LoggerHandler(getFile))
	router.Handle("/api/v1/lorem/{encoding}", LoggerHandler(lorem))
	router.Handle("/api/v1/health", LoggerHandler(healthz))
	router.Handle("/api/v1/bench", benchmark)

	// websocket connections
	router.Handle("/api/v1/chatws", LoggerHandler(chatws))

	// rendered pages
	router.Handle("/chat", LoggerHandler(servePage))
	router.PathPrefix("/video").Handler(http.StripPrefix("/video", http.FileServer(http.Dir("./assets/video"))))
	router.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("./website"))))

	return router
}

// generateFile - used to send back a file where the content-length is in the request param, uses base10 for the amount of bytes, 1GB=1000000000 bytes
// size the size needed in human readable format, e.g. 1KB, 2MB, 3GB, 4TB, 5PB
func generateFile(res http.ResponseWriter, req *http.Request) {
	file := sparse(mux.Vars(req)["size"])
	http.ServeFile(res, req, file)
}

// echoRequest - returns all the request headers and body that was sent over in the request, useful to find out what proxies send through
func echoRequest(res http.ResponseWriter, req *http.Request) {
	headers := map[string]string{}
	var statusCode int = 200
	for name, values := range req.Header {
		for _, value := range values {
			headers[name] = value
		}
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Error(err)
	}

	headers["Host"] = req.Host
	if string(body) != "" {
		headers["Body"] = string(body)
	}

	qs := req.FormValue("status")
	if qs != "" {
		statusCode, _ = strconv.Atoi(qs)
	}

	res.Header().Add("Content-type", "application/json")
	bh, _ := json.Marshal(headers) // byte headers, ready as json
	res.WriteHeader(statusCode)
	res.Write(bh)
}

// httpAuth - basic http authentication, username is user and password is pass
func httpAuth(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)
	user, pass, authOK := req.BasicAuth()

	if authOK == false {
		http.Error(res, "Not authorized", 401)
		return
	}

	if user != "user" || pass != "pass" {
		http.Error(res, "Not authorized", 401)
		return
	}

	res.Header().Add("Content-type", "application/json")
	res.WriteHeader(200)
	res.Write([]byte("{\"success\": true}"))
}

// chunkedRes - used to return a chunked response in 500 millisecond intervals
func chunkedRes(res http.ResponseWriter, req *http.Request) {
	flusher, ok := res.(http.Flusher)
	if !ok {
		log.Error("expected http.ResponseWriter to be an http.Flusher")
	}

	chunks := mux.Vars(req)["chunks"]
	var intC int
	intC, err = strconv.Atoi(chunks)
	if err != nil {
		log.Warnf("Invalid size %s", chunks)
		intC = 10
	}
	res.Header().Set("X-Content-Type-Options", "nosniff")

	for i := 1; i <= intC; i++ {
		fmt.Fprintf(res, "Chunk #%d\n", i)
		flusher.Flush() // Trigger "chunked" encoding and send a chunk
		time.Sleep(500 * time.Millisecond)
	}
}

// loremData - used to return some lorem data in plain text, gzip or brotli compressed format
func loremData(res http.ResponseWriter, req *http.Request) {
	switch encoding := mux.Vars(req)["encoding"]; encoding {
	case "gzip":
		res.Header().Set("Content-Encoding", "gzip")
		http.ServeFile(res, req, "assets/files/lorem.gz")
	case "brotli":
		res.Header().Set("Content-Encoding", "br")
		http.ServeFile(res, req, "assets/files/lorem.br")
	default:
		http.ServeFile(res, req, "assets/files/lorem.txt")
	}
}

// bench - used for benchmarking purposes
func bench(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(200)
	res.Write([]byte("1"))
}

// health - used for liveness endpoints
func health(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(200)
	res.Write([]byte("OK"))
}
