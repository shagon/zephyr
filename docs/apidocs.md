# Documentation

Zephyr works like any other webserver the difference is that it tries to be a feature creep having every functionality available in order to test everything that cannot be easily reproduced or forced from a normal webserver, e.g. timing out on requests.

## API routes

Most of the functionality is beind the `/api/v1` endpoint that we will cover, there are also middlewares built in on each request.

### Middlewares

All `/api/v1/` routes are logged and the request can be seen in the terminal. The parameters logged are:

* `remoteAddr` - the IP address in the request
* `URI` - the path towards the resource
* `method` - HTTP method
* `date` - timestamp of the request
* `userAgent` - user agent used in the request

All routes also have `x-request-id` on the response so its easier to find the specific request that occurred.

There is middleware that is used to add custom response headers provided in the query string. Examples:

!!! example
    ```bash
    $ curl 'https://zephyr.redpulsar.dev/api/v1/echo?some=example' -I
    HTTP/1.1 200 OK
    Access-Control-Allow-Origin: *
    Content-Type: application/json
    Server: zephyr
    Some: example
    X-Request-Id: 20f00226-4400-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:03:13 GMT
    Content-Length: 67
    ```

In the above example we can see that we can force specific response headers, `Some` with the value of `example`. The endpoint used `/api/v1/echo` is flexible but we will get to it in a moment. All routes also support the `after` query parameter that will return the request after specifc amount of time. All endpoints also support `status` parameter that will return that specific status code, combining the two:

!!! example
    ```bash
    $ time curl 'https://zephyr.redpulsar.dev/api/v1/echo?after=3&status=502' -I
    HTTP/1.1 502 Bad Gateway
    Access-Control-Allow-Origin: *
    After: 3
    Content-Type: application/json
    Server: zephyr
    Status: 502
    X-Request-Id: 9ef96d56-4400-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:06:47 GMT
    Content-Length: 67

    real  0m3.231s
    user  0m0.027s
    sys	  0m0.010s
    ```

We can force a 502 status code after a few seconds. With `curl` being the primary motivation for the tool you can add multiple headers without the need to encode the string yourself, for example, adding the `last-modified` response header and the `cache-control` response headers.

!!! example
    ```bash
    $ curl -X GET -I zephyr.redpulsar.dev/api/v1/echo -G --data-urlencode 'Last-Modified=Wed, 25 Oct 2015 12:34:56 GMT' --data-urlencode 'Cache-Control=public, max-age=60'
    HTTP/1.1 200 OK
    Access-Control-Allow-Origin: *
    Cache-Control: public, max-age=60
    Content-Length: 286
    Content-Type: application/json
    Date: Tue, 14 Apr 2020 16:33:22 GMT
    Last-Modified: Wed, 25 Oct 2015 12:34:56 GMT
    Server: zephyr
    X-Request-Id: a7e72107-7e6d-11ea-81cd-0242c0a83002
    ```

Note, the server itself will not respond with a 304 if we use `If-None-Match` or `If-Modified-Since` to check freshness against it, `zephyr` is built for testing purposes not like a general webserver.

### /api/v1/basic

Endpoint is behind basic http authentication, the username is **user** and password is **pass**.

!!! example
    ```bash
    $ curl http://zephyr.redpulsar.dev/api/v1/basic -u user:pass
    HTTP/1.1 200 OK
    Content-Type: application/json
    Server: zephyr
    Www-Authenticate: Basic realm="Restricted"
    X-Request-Id: 59d50074-36a3-11ea-9d91-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:14:15 GMT
    Content-Length: 17
    ```

!!! error
    ```bash
    $ curl http://zephyr.redpulsar.dev/api/v1/basic
    HTTP/1.1 401 Unauthorized
    Access-Control-Allow-Origin: *
    Content-Type: text/plain; charset=utf-8
    Server: zephyr
    Www-Authenticate: Basic realm="Restricted"
    X-Content-Type-Options: nosniff
    X-Request-Id: affaba8c-4401-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:14:22 GMT
    Content-Length: 15
    ```

### /api/v1/chunked/{chunks}

Endpoint is used to test chunked responses, a chunked response is sent in 500 millisecond intervals. Data is sent in a series of chunks. The `Content-Length` header is omitted in this case.

!!! example
    ```bash
    $ curl zephyr.redpulsar.dev/api/v1/chunked/5 -D-
    HTTP/1.1 200 OK
    Access-Control-Allow-Origin: *
    Server: zephyr
    X-Content-Type-Options: nosniff
    X-Request-Id: 236d6cb4-4405-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:39:04 GMT
    Content-Type: text/plain; charset=utf-8
    Transfer-Encoding: chunked

    Chunk #1
    Chunk #2
    Chunk #3
    Chunk #4
    Chunk #5
    ```

### /api/v1/echo

The endpoint returns all the request headers and body that was sent over in the request, useful to find out what procies send through. The response is in json format.

!!! example
    ```bash
    $ curl zephyr.redpulsar.dev/api/v1/echo?good=true -H "x-header: yes" --compressed -D-
    HTTP/1.1 200 OK
    Access-Control-Allow-Origin: *
    Content-Type: application/json
    Good: true
    Server: zephyr
    X-Request-Id: a8b43b5e-4406-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:49:57 GMT
    Content-Length: 118

    {"Accept":"*/*","Accept-Encoding":"deflate, gzip","Host":"zephyr.redpulsar.dev","User-Agent":"curl/7.64.1","X-Header":"yes"}
    ```

### /api/v1/file/{size}

It's used to send back a file where the content-length is in the request param, uses base10 for the amount of bytes, 1GB=1000000000 bytes

size the size needs to be in human readable format, e.g. 1KB, 2MB, 3GB, 4TB, 5PB

!!! example
    ```bash
    $ curl -I http://zephyr.redpulsar.dev/api/v1/file/1GB
    HTTP/1.1 200 OK
    Accept-Ranges: bytes
    Access-Control-Allow-Origin: *
    Content-Length: 1000000000
    Content-Type: application/octet-stream
    Last-Modified: Tue, 14 Jan 2020 17:37:21 GMT
    Server: zephyr
    X-Request-Id: 517f14c0-4407-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:54:41 GMT
    ```

### /api/v1/lorem/{encoding}

It's used to return some lorem ipsum data in plain text, gzip or brotli compressed format. `encoding` value can be any of the following:

* `txt` - (default or invalid value falls back to this) - plain text
* `gzip` - gzipped asset
* `brotli` - compressed using brotli

=== "plain text"
    ``` bash
    $ curl -ILX GET http://zephyr.redpulsar.dev/api/v1/lorem/txt
    HTTP/1.1 200 OK
    Accept-Ranges: bytes
    Access-Control-Allow-Origin: *
    Content-Length: 73713
    Content-Type: text/plain; charset=utf-8
    Last-Modified: Fri, 31 Jan 2020 06:29:35 GMT
    Server: zephyr
    X-Request-Id: 93f4fb44-4407-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:56:32 GMT
    ```

=== "gzip"
    ``` bash
    $ curl -ILX GET http://zephyr.redpulsar.dev/api/v1/lorem/gzip
    HTTP/1.1 200 OK
    Accept-Ranges: bytes
    Access-Control-Allow-Origin: *
    Content-Encoding: gzip
    Content-Type: application/x-gzip
    Last-Modified: Fri, 31 Jan 2020 06:52:08 GMT
    Server: zephyr
    X-Request-Id: 95b7540e-4407-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:56:35 GMT
    Transfer-Encoding: chunked
    ```

=== "brotli"
    ``` bash
    $ curl -ILX GET http://zephyr.redpulsar.dev/api/v1/lorem/brotli
    HTTP/1.1 200 OK
    Accept-Ranges: bytes
    Access-Control-Allow-Origin: *
    Content-Encoding: br
    Content-Type: application/octet-stream
    Last-Modified: Fri, 31 Jan 2020 06:45:19 GMT
    Server: zephyr
    X-Request-Id: 971aba48-4407-11ea-9b34-acbc32782ee7
    Date: Fri, 31 Jan 2020 08:56:37 GMT
    Transfer-Encoding: chunked
    ```

### /api/v1/chatws

In addition to the standard HTTP endpoints there is support for websockes, a fully usable chat application is implemented.

The chat application resides at `https://zephyr.redpulsar.dev/chat` (with some plain html a very small CSS to be usable)

!!! example
    ```bash
    wscat -c 'ws://zephyr.redpulsar.dev/api/v1/chatws'
    connected (press CTRL+C to quit)
    > hello there
    < hello there
    < general kenobi
    ```

### /api/v1/health

Used as an endpoint for readiness or liveness probes, returns "OK"

!!! example
    ```bash
    $ curl -I http://zephyr.redpulsar.dev/api/v1/health
    HTTP/1.1 200 OK
    Access-Control-Allow-Origin: *
    Server: zephyr
    X-Request-Id: ec8b37e6-7c10-11ea-a596-acbc32782ee7
    Date: Sat, 11 Apr 2020 16:24:31 GMT
    Content-Length: 2
    Content-Type: text/plain; charset=utf-8
    ```

### /api/v1/bench

Used for benchmarking purposes, no logging and bare minimum of response headers, returns a single byte of actual data as to reduce overhead

!!! example
    ```bash
    $ curl -I http://zephyr.redpulsar.dev/api/v1/bench
    HTTP/1.1 200 OK
    Date: Sat, 11 Apr 2020 16:17:53 GMT
    Content-Length: 1
    Content-Type: text/plain; charset=utf-8
    ```

## Static routes

In addition to the `/api/v1/` routes there are some static routes as well as some static files used for testing purposes.

### /chat

Fully fledged chat aplication that uses websockets for communication, see [/api/v1/chatws](apidocs.md#apiv1chatws) for the websocket information.

### static files

As the web is comprised of (mostly) static assets some general well used file formats are below.

The list of files themselves is as follows:

| Name         | Type      | Link                                                     |
|--------------|-----------|----------------------------------------------------------|
| BigBuckBunny | hls       | [index.m3u8](/video/bigbuckbunny/index.m3u8)             |
| BigBuckBunny | mp4       | [bigbuckbunny.mp4](/video/bigbuckbunny/bigbuckbunny.mp4) |
| Lorem ipsum  | plaintext | [lorem.txt](/api/v1/lorem/txt)                           |
| Lorem ipsum  | gzip      | [lorem.gz](/api/v1/lorem/gzip)                           |
| Lorem ipsum  | brotli    | [lorem.br](/api/v1/lorem/brotli)                         |