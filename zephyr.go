package main

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/joho/godotenv"
)

// logger - used to log
// ENV - used to hold environment variables
var (
	ENV, err = godotenv.Read()
)

func main() {
	// Output to stdout instead of the default stderr
	log.SetOutput(os.Stdout)
	log.SetLevel(log.InfoLevel)

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	go startHTTPServer()
	go startHTTPSServer()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	log.Warnf("Signal (%v) received, stopping", <-sig)

	stopService()
}

// stopService - stops the http and https server if running
func stopService() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	httpSrv.SetKeepAlivesEnabled(false)
	httpsSrv.SetKeepAlivesEnabled(false)

	// Doesn't block if no connections, but will otherwise wait until the timeout deadline
	if err := httpSrv.Shutdown(ctx); err != nil {
		log.Errorf("Failed to shutdown HTTP server %s", err)
	}

	if err = httpsSrv.Shutdown(ctx); err != nil {
		log.Errorf("Failed to shutdown HTTPS server %s", err)
	}
}
