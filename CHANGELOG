## [1.2.1] - 2020-05-13
### Tests
- tests for routes implemented

### Kubernetes
- added deployment, service and ingress

### Helmchart
- functional helmchart created

### Changes
- changed how the application is getting the real IP address

## [1.2.0] - 2020-04-17
### Updates, fixes, new features

#### Updates
- updated mkdocs to mkdocs-1.1
- updated material skin to mkdocs-material-5.1.0
- updated README.md

#### Features
- added `/api/v1/health` endpoint - used for kubernetes
- added `/api/v1/bench` endpoint - used for benchmarking purposes
- removed `/files` endpoint and rewrote it to use mkdocs table

#### Fixes
- fixed seeing real IP address in logs
- fixed documentation to be consistant with results
- fixed setting custom header, instead of adding it will overwrite

## [1.1.0] - 2020-01-31
### Rework
- Removed unneeded routes
- Added chunked encoding functionality
- Added various compressed files
- Reworked the whole documentation

## [1.0.0] - 2020-01-14
### Added
- Main basic functionality implemented
- gitlab CI automatically builds new images on pushes to master
- Dockerfile support
- added readme / getting started guide
- List of features:
  * online up-to-date documentation with examples and endpoints
  * chat apps with websocket support
  * video files and hls streams
  * echo requests
  * custom large files
  * basic http auth
  * slow responses
  * timeout requests
