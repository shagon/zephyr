package main

import (
	"crypto/tls"
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

var (
	httpsSrv = &http.Server{}
	httpSrv  = &http.Server{}
)

// startHTTPServer - starts a HTTP server if the HTTP port is supplied and not used
func startHTTPServer() {
	if ENV["HTTP_PORT"] != "" { // check if we're using http
		httpSrv = &http.Server{
			Addr:         ":" + ENV["HTTP_PORT"],
			Handler:      router,
			ReadTimeout:  time.Minute,
			WriteTimeout: time.Minute,
		}

		log.Infof("Starting http server on port %s", ENV["HTTP_PORT"])

		if err = httpSrv.ListenAndServe(); err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}
}

// startHTTPSServer - starts a https server if the cert, key and the port are supplied (and the port is not used)
func startHTTPSServer() {
	if ENV["HTTPS_PORT"] != "" && ENV["SSL_CERT"] != "" && ENV["SSL_KEY"] != "" { // check if we should enable https
		httpsSrv = &http.Server{
			Addr:         ":" + ENV["HTTPS_PORT"],
			Handler:      router,
			TLSConfig:    &tls.Config{NextProtos: []string{"h2"}},
			ReadTimeout:  time.Minute,
			WriteTimeout: time.Minute,
		}

		log.Infof("Starting https server on port %s", ENV["HTTPS_PORT"])

		if err = httpsSrv.ListenAndServeTLS(
			ENV["SSL_CERT"],
			ENV["SSL_KEY"],
		); err != http.ErrServerClosed {
			log.Fatal(err)
		}
	}
}
