package main

import (
	"fmt"
	"html/template"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	uuid "github.com/google/uuid"
	log "github.com/sirupsen/logrus"
)

type location struct {
	Domain string
	Socket string
}

// LoggerHandler - middleware - used to log the URI and the IP address from the request
func LoggerHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		requestID, _ := uuid.NewUUID()

		log.WithFields(log.Fields{
			"remoteAddr": getIPAddress(req),
			"URI":        req.RequestURI,
			"method":     req.Method,
			"date":       time.Now().Format(time.RFC3339),
			"userAgent":  req.UserAgent(),
		}).Infof("requestID=%s", requestID)

		res.Header().Add("x-request-id", requestID.String())
		res.Header().Set("Access-Control-Allow-Origin", "*")
		res.Header().Add("Server", "zephyr")

		// inject any custom headers from query string
		customHeaders(res, req)
		// return the request after x amount of time
		throttleRequest(req)
		next.ServeHTTP(res, req)
	})
}

// customHeaders - middleware used to add custom response headers provided in the query string
func customHeaders(res http.ResponseWriter, req *http.Request) {
	for name, values := range req.URL.Query() {
		for _, value := range values {
			res.Header().Set(name, value)
		}
	}
}

// templateHTML - rewrites the location and the protcol in the html to the environment variables provided
func templateHTML(res http.ResponseWriter, req *http.Request) {
	page, _ := template.ParseFiles(fmt.Sprintf("./assets%s/index.html", req.RequestURI))
	var (
		socket string
	)
	if req.TLS != nil {
		socket = "wss"
	} else {
		socket = "ws"
	}

	page.Execute(res, location{
		Domain: req.Host,
		Socket: socket,
	})
}

// throttleRequest - returns the data after x amount of time
func throttleRequest(req *http.Request) {
	after := req.FormValue("after")
	if after != "" {
		dur, _ := strconv.Atoi(after)
		time.Sleep(time.Second * time.Duration(dur))
	}
}

// getIPAddress - used to return the IP address of the request
func getIPAddress(req *http.Request) string {
	xRealIP := req.Header.Get("X-Real-Ip")
	xForwardedFor := req.Header.Get("X-Forwarded-For")

	// if X-Real-IP exists return it otherwise use X-Forwarded-For
	if xRealIP != "" {
		return xRealIP
	} else if xForwardedFor != "" {
		return xForwardedFor
	}

	// if both don't exist fallback to "RemoteAddr" from request
	rAddr := req.RemoteAddr
	if strings.ContainsRune(rAddr, ':') {
		var clientIP string
		clientIP, _, _ = net.SplitHostPort(rAddr)
		return clientIP
	}
	return rAddr
}
